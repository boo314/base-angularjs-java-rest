studentsApp.factory('studentService', [ '$http', '$log', function($http, $log) {

	return {
		allStudents : function($scope) {

			$http({
				method : 'GET',
				url : '/rest/students/all'
			}).success(function(data, status, headers, config) {
				$scope.students = data.personShort;
			}).error(function(data, status, headers, config) {
				$log.info("Rest service ERROR");
			});
		},
		addStudent : function($scope) {
			$http({
				method : 'POST',
				url : '/rest/students/add'
			}).success(function(data, status, headers, config) {
				console.log(data);
			}).error(function(data, status, headers, config) {
				console.log("Add student error");
			});

		},
		studentDetails : function($scope, id) {

			$http({
				method : 'GET',
				url : '/rest/students/' + id
			}).success(function(data, status, headers, config) {
				$scope.selectedStudent = data;
			}).error(function(data, status, headers, config) {
				console.log("Rest service ERROR");
			});
		},
		removeStudent : function($scope, id) {
			$http({
				method : 'DELETE',
				url : '/rest/students/remove/' + id
			}).success(function(data, status, headers, config) {
				console.log(data);
			}).error(function(data, status, headers, config) {
				console.log("Remove student error");
			});

		}
		
	};

} ]);