var studentsApp = angular.module('studentsApp', [
    'ngRoute',
    'studentsAppControllers'
]);

studentsApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/students', {
                templateUrl: 'partials/student-list.html',
                controller: 'StudentListCtrl'
            }).
            when('/student/add', {
                templateUrl: 'partials/student-add.html',
                controller: 'StudentAddCtrl'
            }).
            when('/student/:studentId', {
                templateUrl: 'partials/student-details.html',
                controller: 'StudentDetailCtrl'
            }).
            otherwise({
                redirectTo: '/students'
            });
    }]);


