var studentsAppControllers = angular.module('studentsAppControllers', []);

studentsAppControllers.controller('StudentListCtrl', [ '$scope',
		'studentService', function($scope, studentService) {

			$scope.students = [];
			studentService.allStudents($scope);
			$scope.getAllStudents = function() {
				studentService.allStudents($scope);
			};

		} ]);

studentsAppControllers.controller('StudentDetailCtrl', [ '$scope',
		'$routeParams', 'studentService',

		function($scope, $routeParams, studentService) {
			var personId = $routeParams.studentId;
			studentService.studentDetails($scope, personId);

			$scope.removeStudent = function() {
				studentService.removeStudent($scope, personId);

			};

		} ]);

studentsAppControllers.controller('StudentAddCtrl',
		[
				'$scope',
				'$routeParams',
				'studentService',

				function($scope, $routeParams, studentService) {
					
					
				}

		]);