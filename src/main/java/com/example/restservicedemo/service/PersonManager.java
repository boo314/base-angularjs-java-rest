package com.example.restservicedemo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.example.restservicedemo.domain.PersonDetails;
import com.example.restservicedemo.domain.PersonShort;

@Path("students")
public class PersonManager {
	
	private Map<Long, PersonDetails> storage = new HashMap<Long, PersonDetails>();

	public PersonManager(){
		storage.put(1L, new PersonDetails(1,"Bolek", "", 1967, true, "bolek@wp.pl"));
		storage.put(2L, new PersonDetails(2,"Lolek", "", 1968, true, "lolek@o2.pl"));
		storage.put(3L, new PersonDetails(3,"Tola", "", 1969, false, "tola@op.pl"));
	}
	
	@GET
	@Path("/{studentId}")
	@Produces("application/json")
	public PersonDetails getPerson(@PathParam("studentId") Long id){
		PersonDetails p = storage.get(id);
		if (p == null) return new PersonDetails();
		return p;
	}
	
	
	@GET
	@Path("/all")
	@Produces("application/json")
	public List<PersonShort> getAllPerson(){
		List<PersonShort> persons = new ArrayList<PersonShort>();
		
		for (Long id : storage.keySet()) {
			PersonShort ps = new PersonShort();
			ps.setId(id);
			ps.setFirstName(storage.get(id).getFirstName());
			persons.add(ps);
		}
		
		return persons;
	}
	
	
	@DELETE
	@Path("/remove/{studentId}")
	public String removePerson(@PathParam("studentId") Long id){
		try{
			storage.remove(id);
			for(Long p : storage.keySet()){
				System.out.println("[Test] "+p);
			}
			
			return "Usuniety";
		}catch(Exception e){
			e.printStackTrace();
		}
		return "Blad";
	}
	
	@POST
	@Path("/add")
	public String addPerson(){
		
		return "Blad";
	}
	
	@GET
	@Path("/test")
	@Produces("text/html")
	public String test(){
		return "REST Service is running";
	}

}
