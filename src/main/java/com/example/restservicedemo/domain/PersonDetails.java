package com.example.restservicedemo.domain;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
public @Data class PersonDetails {
	
	private int id;
	private String firstName;
	private String lastName;
	private int yob;
	private boolean sex;
	private String email;
	
	
	
	
	
//	public PersonDetails() {
//	}
//
//	public PersonDetails(String firstName, int yob) {
//		this.firstName = firstName;
//		this.yob = yob;
//	}
//	
//	public String getFirstName() {
//		return firstName;
//	}
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//	public int getYob() {
//		return yob;
//	}
//	public void setYob(int yob) {
//		this.yob = yob;
//	}
}
